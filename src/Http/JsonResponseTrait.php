<?php


namespace Ucc\Http;


trait JsonResponseTrait
{
    /**
     * Json helper to print out response
     * @param $data
     * @param  int  $statusCode
     * @return string
     */
    public function json($data, int $statusCode = 200): string
    {
        http_response_code($statusCode);
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json;charset=utf-8');
        return json_encode($data);
    }
}
