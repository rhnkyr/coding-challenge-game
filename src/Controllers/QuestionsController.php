<?php


    namespace Ucc\Controllers;


    use Ucc\Services\QuestionService;
    use Ucc\Session;
    use Ucc\Http\JsonResponseTrait;

    class QuestionsController extends Controller
    {
        use JsonResponseTrait;

        private QuestionService $questionService;

        public function __construct(QuestionService $questionService)
        {
            parent::__construct();
            $this->questionService = $questionService;
        }

        /**
         * Game initializer
         * @return string
         */
        public function beginGame(): string
        {

            $name = $this->requestBody->name ?? null;

            if (empty($name)) {
                return $this->json(['message' => 'You must provide a name'], 400);
            }

            Session::set('name', $name);
            Session::set('questionCount', 1);
            Session::set('points', 0);

            $question = $this->getQuestion();

            return $this->json(['question' => $question], 201);
        }

        private function getQuestion(): array
        {
            return $this->questionService->getRandomQuestions(1);
        }

        /**
         * Calculates points and generate new question
         *
         * @param  int  $id
         * @return string
         * @throws \KHerGe\JSON\Exception\DecodeException
         * @throws \KHerGe\JSON\Exception\UnknownException
         */
        public function answerQuestion(int $id): string
        {


            $points = Session::get('points');

            $answer = $this->requestBody->answer ?? null;
            if (empty($answer)) {
                return $this->json(['message' => 'You must provide an answer'], 400);
            }

            if (Session::get('name') === null) {
                return $this->json(['message' => 'You must provide a name'], 400);
            }


            $questionCount = Session::get('questionCount');
            $questionCount++;
            //Set current question count
            Session::set('questionCount', $questionCount);

            //Check answer by given question id
            $result = $this->questionService->getPointForAnswer($id, $answer);

            //Calculate total points
            $total_points = $points + $result;

            if ((int) Session::get('questionCount') > 4) {
                $name = Session::get('name');
                Session::destroy();

                return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$total_points} points!"]);
            }

            //Set total point on session
            Session::set('points', $total_points);

            $message = [
                'total_points'    => $total_points,
                'previous_answer' => $answer
            ];

            //Todo : find unasked question
            $question = $this->getQuestion();

            return $this->json(['message' => $message, 'question' => $question]);
        }
    }
