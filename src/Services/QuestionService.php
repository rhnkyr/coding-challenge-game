<?php


    namespace Ucc\Services;


    use JsonMapper;
    use KHerGe\JSON\JSON;

    class QuestionService
    {
        private const QUESTIONS_PATH = __DIR__.'/../../questions.json';

        private JSON       $json;
        private JsonMapper $jsonMapper;

        public function __construct(JSON $json, JsonMapper $jsonMapper)
        {
            $this->json = $json;
            $this->jsonMapper = $jsonMapper;
        }

        /**
         * Generates random question(s)
         * @param  int  $count
         * @return array
         * @throws \KHerGe\JSON\Exception\DecodeException
         * @throws \KHerGe\JSON\Exception\UnknownException
         */
        public function getRandomQuestions(int $count = 5): array
        {
            $questions = $this->json->decodeFile(self::QUESTIONS_PATH, true);

            //Shuffle questions
            shuffle($questions);

            //Get first question
            return array_slice($questions, 0, $count);
        }

        /**
         * Checks answer by given question id
         * @param  int  $id
         * @param  string  $answer
         * @return int
         * @throws \KHerGe\JSON\Exception\DecodeException
         * @throws \KHerGe\JSON\Exception\UnknownException
         */
        public function getPointForAnswer(int $id, string $answer): int
        {
            $questions = $this->json->decodeFile(self::QUESTIONS_PATH);

            $correct = '';
            $points = 0;

            foreach ($questions as $question) {
                if ($question->id === $id) {
                    $correct = $question->correctAnswer;
                    $points = $question->points;
                    break;
                }
            }

            return $answer === $correct ? $points : 0;

        }
    }
